# Dots

## Directory
```
.dots/
├── assets
│   ├── backgrounds
│   │   └── eva-bg.jpg
│   └── readme
│       └── script-options.png
├── LICENSE
├── README.md
└── scripts
    ├── directory-helpers.sh
    ├── fresh-os-setup-script.sh
    └── print-helpers.sh
```

## Scripts
All scripts are located under /scripts

### Clean setup script
`fresh-os-setup-script.sh`
This script handles handles app installs, git setup, theme and styling setup, etc. Ideally it's only used once to setup a clean Linux installation. Only supports apt and flatpak.

#### Command Options
- s This skips the initial prompt and goes through all available options.
	Example: `./fresh-os-setup-script.sh -s`

#### Script Options
![Script Options](./assets/readme/script-options.png)

1. Goes through every process available
2. Creates a random seed
3. Creates directories and CDs to the home dots directory
4. Currently sets the background image
5. Downloads packages to `$HOME/.dots/temp` and installs them
6. Sets git configuration
7. Removes any temp files created

### Printing
`print-helpers.sh`
This script has various methods to print out messages with style using printf

### Directory
`directory-helpers.sh`
This script has methods to handle directory creation
