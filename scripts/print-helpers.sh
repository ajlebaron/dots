#!/bin/bash

#region Methods

function print_welcome {
    printf "$GREEN\n-----------------------------------------------------------------------\n"
    printf "$GREEN\n--------------------[ AUTOMATIZED CLEAN OS SCRIPT ]--------------------\n"
    printf "$GREEN\n-----------------------------------------------------------------------\n$NORMAL"
}

function print_exit {
    printf "$GREEN\n-------------------------------------------------------------\n"
    printf "$GREEN\n--------------------[ SETUP COMPLETE!!! ]--------------------\n"
    printf "$GREEN\n-------------------------------------------------------------\n$NORMAL"
}

function print_title {
    printf "$YELLOW\n--------------------[ $1 ]--------------------\n$NORMAL"
}

function print_message {
    printf "\n> $1\n$NORMAL"
}

#endregion
