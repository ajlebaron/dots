#!/bin/bash

#region Sources

. ./print-helpers.sh --source-only
. ./directory-helpers.sh --source-only

#endregion

#region Variables

# Application URLS
CHROME_URL="https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"
VSCODE_URL="https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-x64"

# Filenames
CHROME_FILENAME="chrome.deb"
VSCODE_FILENAME="vscode.deb"
BG_IMAGE="file://$HOME/.dots/assets/backgrounds/eva-bg.jpg"

# Directories
DOTS_DIR="$HOME/.dots"
SUB_DOTS_DIR="temp"
WORKSPACE_DIR="$HOME/workspace"

# COLORS
BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
LIME_YELLOW=$(tput setaf 190)
POWDER_BLUE=$(tput setaf 153)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
BRIGHT=$(tput bold)
NORMAL=$(tput sgr0)
BLINK=$(tput blink)
REVERSE=$(tput smso)
UNDERLINE=$(tput smul)

# Git
NAME="Alberto Jaime Le Baron Velazco"
EMAIL="jimmleb178@gmail.com"

# Other
SEED=""
INITIAL_SELECTED_OPTIONS=("1" "2" "3" "4" "5" "6" "7")
SELECTED_OPTIONS=("1" "2" "3" "4" "5" "6" "7")
OPTION_TITLES=("SELECT ALL" "INITIAL SETUP" "DIRECTORIES SETUP" "SETUP OS THEMES AND STYLES" "GET AND INSTALL APPLICATIONS" "GIT SETUP" "CLEAN UP")

for ARG in "$@"; do
    USER_ARGUMENTS+=("$ARG")
done

#endregion

#region Methods

function on_init { #2
    clear
    print_welcome
    sleep 1s

    print_message "creating seed"
    SEED=$(echo $RANDOM | md5sum | head -c 20)
}

function setup_dirs { #3
    print_title "SETUP DIRECTORIES"

    # Checks if temp directory exists. If true do nothing, if false create the dir and cd into it
    create_dir "$DOTS_DIR/$SUB_DOTS_DIR"
    create_dir "$WORKSPACE_DIR"

    print_message "copying files"
    cd ..
    cp -r * $DOTS_DIR

    print_message "changing directories"
    cd $DOTS_DIR
}

function setup_os_theme { #4
    print_title "SET STYLES AND THEMES"

    print_message "checking if valid OS to setup wallpaper"
    IS_DEBIAN_BASED="$(lsb_release -d | grep -E -i --count '(Pop!|Ubuntu)')"
    if [ "$IS_DEBIAN_BASED" -eq "1" ]; then
        gsettings set org.gnome.desktop.background picture-uri $BG_IMAGE
        gsettings set org.gnome.desktop.background picture-uri-dark $BG_IMAGE

        print_message "wallpaper set"
    fi
}

function get_packages { #5
    print_title "DOWNLOADING APPLICATIONS"

    print_message "cd into temp directory"
    cd temp

    print_message "updating local"
    sudo apt update
    sudo apt upgrade -y

    print_message "downloading packages"
    print_message "downloading google chrome stable..$BLINK."
    curl -L $CHROME_URL -o $CHROME_FILENAME
    print_message "downloading visual studio code..$BLINK."
    curl -L $VSCODE_URL -o $VSCODE_FILENAME
}

function install_packages {
    print_title "INSTALLING PACKAGES"

    # installs debian packages
    [ -f ./$CHROME_FILENAME ] && sudo apt -f install ./$CHROME_FILENAME
    [ -f ./$VSCODE_FILENAME ] && sudo apt -f install ./$VSCODE_FILENAME

    IS_DEBIAN_BASED="$(lsb_release -d | grep -E -i --count '(Pop!|Ubuntu)')"
    if [ "$IS_DEBIAN_BASED" -eq "1" ]; then
        sudo apt install -y neofetch tree
    fi

    # installs flatpak packages
    IS_FLATPAK_AVAILABLE=$(apt-cache policy flatpak | grep Installed)
    if [ "$IS_FLATPAK_AVAILABLE" != "" ]; then
        flatpak install -y app/md.obsidian.Obsidian/x86_64/stable
    fi

    # installs terminal packages
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.4/install.sh | bash
    nvm install node

    cd ..
}

function setup_git { #6
    print_title "SETUP GIT CONFIGURATION"

    LOCAL_GIT_NAME="$(git config --list | grep user.name)"
    LOCAL_GIT_EMAIL="$(git config --list | grep user.email)"
    if [ "$LOCAL_GIT_NAME" == "" ] && [ "$LOCAL_GIT_EMAIL" == "" ]; then
        print_message "Updating git settings"
        git config --global user.name "$NAME"
        git config --global user.email "$EMAIL"
        print_message "Git globals set"
    else
        print_message "Git globals already set"
    fi

    print_message "Create SSH Key"
    yes '' | ssh-keygen -t ed25519 -C "$SEED" -N ''
}

function on_destroy { #7
    print_title "CLEAN UP"

    print_message "removing temp files"
    rm -rf temp

    print_exit
    sleep 1s
    exit
}

function run_method {
    case $1 in
    2)
        on_init
        ;;
    3)
        setup_dirs
        ;;
    4)
        setup_os_theme
        ;;
    5)
        get_packages
        install_packages
        ;;
    6)
        setup_git
        ;;
    7)
        on_destroy
        ;;
    esac
}

function selected_option_handler {
    case $1 in
    1)
        if [[ " ${SELECTED_OPTIONS[@]} " =~ " "$1" " ]]; then
            SELECTED_OPTIONS=()
        else
            SELECTED_OPTIONS=("${INITIAL_SELECTED_OPTIONS[@]}")
        fi
        ;;
    2)
        if [[ " ${SELECTED_OPTIONS[@]} " =~ " "$1" " ]]; then
            SELECTED_OPTIONS=("${SELECTED_OPTIONS[@]/$1/}")
            SELECTED_OPTIONS=("${SELECTED_OPTIONS[@]/"1"/}")
            SELECTED_OPTIONS=("${SELECTED_OPTIONS[@]/"6"/}")
        else
            SELECTED_OPTIONS+=($1)
        fi
        ;;
    3)
        if [[ " ${SELECTED_OPTIONS[@]} " =~ " "$1" " ]]; then
            SELECTED_OPTIONS=("${SELECTED_OPTIONS[@]/$1/}")
            SELECTED_OPTIONS=("${SELECTED_OPTIONS[@]/"1"/}")
        else
            SELECTED_OPTIONS+=($1)
        fi
        ;;
    4)
        if [[ " ${SELECTED_OPTIONS[@]} " =~ " "$1" " ]]; then
            SELECTED_OPTIONS=("${SELECTED_OPTIONS[@]/$1/}")
            SELECTED_OPTIONS=("${SELECTED_OPTIONS[@]/"1"/}")
        else
            SELECTED_OPTIONS+=($1)
        fi
        ;;
    5)
        if [[ " ${SELECTED_OPTIONS[@]} " =~ " "$1" " ]]; then
            SELECTED_OPTIONS=("${SELECTED_OPTIONS[@]/$1/}")
            SELECTED_OPTIONS=("${SELECTED_OPTIONS[@]/"1"/}")
        else
            SELECTED_OPTIONS+=($1)
        fi
        ;;
    6)
        if [[ " ${SELECTED_OPTIONS[@]} " =~ " "$1" " ]]; then
            SELECTED_OPTIONS=("${SELECTED_OPTIONS[@]/$1/}")
            SELECTED_OPTIONS=("${SELECTED_OPTIONS[@]/"1"/}")
        else
            [[ " ${SELECTED_OPTIONS[@]} " =~ " "2" " ]] || SELECTED_OPTIONS+=("2")
            SELECTED_OPTIONS+=($1)
        fi
        ;;
    7)
        if [[ " ${SELECTED_OPTIONS[@]} " =~ " "$1" " ]]; then
            SELECTED_OPTIONS=("${SELECTED_OPTIONS[@]/$1/}")
            SELECTED_OPTIONS=("${SELECTED_OPTIONS[@]/"1"/}")
        else
            SELECTED_OPTIONS+=($1)
        fi
        ;;
    esac
}

USER_ARGUMENTS

function main {
    USER_INPUT="0"
    SELECTED_OPTIONS=("${INITIAL_SELECTED_OPTIONS[@]}")
    if [[ " ${USER_ARGUMENTS[@]} " =~ " "-s" " ]]; then
        USER_OPTED_SKIP="1"
    fi
    while [ "$USER_INPUT" != "" ] && [ "$USER_OPTED_SKIP" != "1" ]; do
        clear
        print_title "SELECT OPTIONS FOR SETUP"
        printf "\n"

        for i in "${!INITIAL_SELECTED_OPTIONS[@]}"; do
            [[ " ${SELECTED_OPTIONS[@]} " =~ " "${INITIAL_SELECTED_OPTIONS[$i]}" " ]] && echo "[X] $(($i + 1)): ${OPTION_TITLES[$i]}" || echo "[ ] $(($i + 1)): ${OPTION_TITLES[$i]}"
        done
        printf "\n"
        read -p "Select option: " USER_INPUT

        selected_option_handler $USER_INPUT
    done

    for i in "${SELECTED_OPTIONS[@]}"; do
        run_method "$i"
    done

}

#endregion

#region Run

main

#endregion
