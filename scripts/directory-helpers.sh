#!/bin/bash

#region Methods

function create_dir {
    print_message "creating directories for $1"

    if [ -d "$1" ]; then
        print_message "Directory $1 exists." 
    else
        for _i in "$1"
        do
            [ ! -d "$_i" ] &&  mkdir -p "$_i"
        done
    fi
}

#endregion
